module.exports = {
	devServer: {
		proxy: "http://localhost:3000"
	},
	chainWebpack: config => {
		if (process.env.NODE_ENV === "development") {
			config.output.filename("[name].[hash].js").end();
		}
	}
};

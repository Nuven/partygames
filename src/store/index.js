import Vue from "vue";
import Vuex from "vuex";

import Socket from "socket.io-client";
import auth from "./auth";
import chat from "./chat";
import game from "./game";
import whoami from "./games/whoami";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		io: Socket()
	},
	modules: {
		auth,
		chat,
		game,
		whoami
	}
});

export default {
	state: {
		messages: [
			{
				user: {
					user_name: "First User"
				},
				message: "Test message"
			},
			{
				user: {
					user_name: "Second User"
				},
				message: "Another message"
			},
			{
				user: {
					user_name: "Third User"
				},
				message: "asdasdawd 2e21 12d12e"
			}
		],
		users: []
	},

	getters: {
		getAllMessages: state => state.messages,
		getUsers: state => state.users
	},

	mutations: {
		ADD_MESSAGE: (state, [user, message]) => {
			console.log(user, message);
			state.messages.push({ user, message });
		},
		ADD_USER_TO_USERS: (state, user) => {
			console.log(user, "ASDASDASD");
			state.users.push(user);
		},
		REMOVE_USER_FROM_USERS: (state, user) => {
			state.users = state.users.filter(
				item => JSON.stringify(item) !== JSON.stringify(user)
			);
		}
	},

	actions: {
		sendMessage: ({ commit, rootState, getters }, message) => {
			rootState.io.emit("user send a message", [
				getters.getUser,
				message
			]);
		},
		addMessageToChat: ({ commit, rootState, getters }, data) => {
			commit("ADD_MESSAGE", data);
		},
		addUserToChat: ({ commit }, user) => {
			commit("ADD_USER_TO_USERS", user);
		},
		addUsersToChat: ({ commit }, usersOnline) => {
			console.log(usersOnline);
			for (let user of usersOnline) {
				commit("ADD_USER_TO_USERS", user);
			}
		},
		reomoveUserFromChat: ({ commit }, user) => {
			commit("REMOVE_USER_FROM_USERS", user);
		}
	}
};

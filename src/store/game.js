export default {
	state: {
		game_name: "",
		game_id: 0,
		creator: "",
		users: [],
		logic: [],
		started: false,
		startedBy: ''
	},

	getters: {
		getGame: state => {
			return {
				creator: state.creator,
				game_id: state.game_id,
				game_name: state.game_name,
				users: state.users,
				logic: state.logic,
				started: state.started
			};
		}
		//getGames: state =>
	},
	mutations: {
		CREATE_A_GAME: (state, [name, user]) => {
			state.game_id = Math.round(Math.random() * 1000);
			state.game_name = name;
			state.creator = user;
		},

		ADD_USER_TO_GAME: (state, user) => {
			state.users.push(user);
		},

		START_A_GAME: (state, startedBy) => {
			state.started = true;
			state.startedBy = startedBy;
		},

		REMOVE_USER_FROM_GAME: (state, user) => {
			state.users = state.users.filter(
				item => JSON.stringify(item) !== JSON.stringify(user)
			);
		},

		DELETE_A_GAME: (state, game_id) => {
			state.started = false;
			console.log("DASD", state.started);
		}
	},

	actions: {
		createGame: ({ commit, rootState, getters }, name) => {
			commit("CREATE_A_GAME", [name, getters.getUser]);
			rootState.io.emit("game was created", getters.getGame);
		},
		joinToGame: ({ commit, rootState, getters }, game_id) => {
			rootState.io.emit("user enter the game", {
				game_id,
				user: getters.getUser
			});
		},
		leaveTheGame: ({ rootState }, game_id) => {
			rootState.io.emit("user leave the game", { game_id });
		},
		startTheGame: ({ rootState, getters, commit }, game_id) => {
			commit("START_A_GAME", getters.getUser);
			rootState.io.emit("start the game", {
				game_id,
				user: getters.getUser
			});
		},
		deleteTheGame: ({ rootState, commit }, game_id) => {
			commit("DELETE_A_GAME", game_id);
			rootState.io.emit("delete the game", {
				game_id
			});
		}
	}
};

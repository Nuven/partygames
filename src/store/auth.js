export default {
	state: {
		user: localStorage.user ? JSON.parse(localStorage.getItem("user")) : "",
		userName: "",
		usersOnline: []
	},
	getters: {
		getUser: state => state.user,
		isLogged: state => Boolean(state.user)
	},
	mutations: {
		changeName: (state, new_name) => {
			state.userName = new_name;
		},

		setUser: state => {
			let user = {
				user_id: Math.round(Math.random() * 100),
				user_name: state.userName
			};
			state.user = user;
			localStorage.setItem("user", JSON.stringify(user));
			state.usersOnline.push(state.user);
		},
		removeUser: state => {
			state.usersOnline = state.usersOnline.filter(
				user => JSON.stringify(user) !== JSON.stringify(state.user)
			);
			localStorage.user = state.user = null;
		}
	},
	actions: {
		addUser: ({ commit, rootState, getters }, new_name) => {
			commit("changeName", new_name);
			commit("setUser");

			rootState.io.emit("login", getters.getUser);
		},
		logout: ({ commit, rootState, getters }) => {
			rootState.io.emit("logout", getters.getUser);
			commit("removeUser");
		}
	}
};

export default {
	actions: {
		addBadgeToUser: ({ rootState }, data) => {
			console.log(data.badge_text);
			rootState.io.emit("add badge to user", {
				//game_id,
				target_id: data.target_id,
				badge_text: data.badge_text
			});
		}
	}
};

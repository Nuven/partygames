let locationsArray = [
	{
		location: "Ресторан",
		roles: [
			"музыкант",
			"посетитель",
			"вышибала",
			"метрдотель",
			"шеф-повар",
			"официант",
			"критик"
		]
	},
	{
		location: "Спа-салон",
		roles: [
			"клиент",
			"стилист",
			"массажист",
			"визажист",
			"маникюрщик",
			"косметолог",
			"дерматолог"
		]
	},
	{
		location: "Отель",
		roles: [
			"бармен",
			"постоялец",
			"горничная",
			"швейцар",
			"охранник",
			"управляющий",
			"портье"
		]
	},
	{
		location: "Университет",
		roles: [
			"студент",
			"вахтёр",
			"ректор",
			"профессор",
			"аспирант",
			"психолог",
			"декан"
		]
	},
	{
		location: "Банк",
		roles: [
			"клиент",
			"охранник",
			"грабитель",
			"кассир",
			"управляющий",
			"инкассатор",
			"консультант"
		]
	},
	{
		location: "Больница",
		roles: [
			"терапевт",
			"хирург",
			"медсестра",
			"пациент",
			"патологоанатом",
			"главврач",
			"интерн",
			"программист"
		]
	},
	{
		location: "Посольство",
		roles: [
			"дипломат",
			"посол",
			"чиновник",
			"секретарь",
			"охранник",
			"турист",
			"беженец"
		]
	},
	{
		location: "Киностудия",
		roles: [
			"звукооператор",
			"каскадёр",
			"оператор",
			"костюмер",
			"режиссёр",
			"актёр",
			"статист"
		]
	},
	{
		location: "Цирк-шапито",
		roles: [
			"дрессировщик",
			"посетитель",
			"фокусник",
			"жонглёр",
			"клоун",
			"метатель ножей",
			"акробат"
		]
	},
	{
		location: "Театр",
		roles: [
			"гардеробщик",
			"суфлёр",
			"билетёр",
			"зритель",
			"режиссёр",
			"актёр",
			"рабочий сцены"
		]
	},
	{
		location: "Церковь",
		roles: [
			"священник",
			"грешник",
			"хорист",
			"прихожанин",
			"нищий",
			"турист",
			"меценат"
		]
	},
	{
		location: "Овощебаза",
		roles: [
			"санинспектор",
			"охранник",
			"грузчик",
			"бухгалтер",
			"бригадир",
			"лаборант",
			"водитель"
		]
	},
	{
		location: "Супермаркет",
		roles: [
			"мерчендайзер",
			"охранник",
			"промоутер",
			"кассир",
			"мясник",
			"уборщик",
			"покупатель"
		]
	},
	{
		location: "Полицейский участок",
		roles: [
			"журналист",
			"детектив",
			"адвокат",
			"криминалист",
			"архивариус",
			"патрульный",
			"преступник"
		]
	},
	{
		location: "Корпоративная вечеринка",
		roles: [
			"менеджер",
			"ведущий",
			"незваный гость",
			"шеф",
			"секретарь",
			"бухгалтер",
			"курьер"
		]
	},
	{
		location: "Океанский лайнер",
		roles: [
			"богатый пассажир",
			"стюард",
			"капитан",
			"бармен",
			"музыкант",
			"кок",
			"радист"
		]
	},
	{
		location: "Подводная лодка",
		roles: [
			"электромеханик",
			"матрос",
			"командир",
			"штурман",
			"гидроакустик",
			"кок",
			"радист"
		]
	},
	{
		location: "Станция техобслуживания",
		roles: [
			"мастер-приёмщик",
			"шиномонтажник",
			"мойщик",
			"мотоциклист",
			"электрик",
			"директор",
			"автомобилист"
		]
	},
	{
		location: "Полярная станция",
		roles: [
			"начальник экспедиции",
			"геофизик",
			"биолог",
			"метеоролог",
			"медик",
			"гидролог",
			"радист"
		]
	},
	{
		location: "Пляж",
		roles: [
			"фотограф с обезьянкой",
			"аниматор",
			"отдыхающий",
			"парапланерист",
			"разносчик еды",
			"спасатель",
			"вор"
		]
	}
];

class SpyFallGame {
	constructor(users, locationsAndRoles = locationsArray) {
		this._users = users;
		this._locationsAndRoles = locationsAndRoles;
		this._game = [];
	}

	getGame() {
		return this._game;
	}

	getRandomLocation() {
		return this._locationsAndRoles[
			Math.floor(Math.random() * this._locationsAndRoles.length)
		];
	}

	getLocations() {
		return locationsArray.map(x => x.location);
	}

	getRandomUser() {
		return this._users[Math.floor(Math.random() * this._users.length)];
	}

	setupGame() {
		let randUser = this.getRandomUser();
		let randLocationAndRoles = this.getRandomLocation();
		let users = this._users;

		let randLocation = randLocationAndRoles.location;
		let roles = randLocationAndRoles.roles;
		let game = [];
		for (let i = 0; i < users.length; i++) {
			let randRole = roles
				.splice([Math.floor(Math.random() * roles.length)], 1)
				.join();

			game.push({
				user: users[i],
				location: randLocation,
				spy: users[i].user_id === randUser.user_id ? true : false,
				role: users[i].user_id === randUser.user_id ? "" : randRole
			});
		}
		this._game = game;
	}

	Start() {
		this._game = [];
		this.setupGame();
	}

	Finish() {
		this._game = [];
	}

	Test() {
		this._users = [
			{
				user_name: "First",
				user_id: 1
			},
			{
				user_name: "Second",
				user_id: 2
			},
			{
				user_name: "Third",
				user_id: 3
			}
		];
		this.Start();
	}
}

module.exports = SpyFallGame;

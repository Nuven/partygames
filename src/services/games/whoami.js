class WhoAmIGame {
	constructor(users) {
		this._users = users;
		this._isStarted = false;
		this._game = [];
	}

	getUsers() {
		return this._users;
	}

	getGame() {
		return this._game;
	}

	IsStarted() {
		return this._isStarted;
	}

	Close() {}

	getRandomUserInGame(socket_user) {
		let users = this.getUsers.filter(
			user => JSON.stringify(user) !== JSON.stringify(socket_user)
		);

		let randUser = users[Math.floor(Math.random() * users.length)];
		console.log("RAND USER " + randUser);
		return randUser;
	}

	generateGame() {
		let usersInGame = this.getUsers();
		let usersToPick = usersInGame;
		let selectedUsers = [];

		for (let i = 0; i < usersInGame.length; i++) {
			this._game.push({
				user: usersInGame[i],
				target:
					i + 1 === usersInGame.length
						? usersInGame[0]
						: usersInGame[i + 1],
				target_text: "",
				users: usersInGame
			});
		}
		this._isStarted = true;
	}

	setBadgeToUser(user_id, text) {
		this.getGame().forEach(val => {
			if (val.user.user_id === user_id) val.target_text = text;
		});
	}

	Start() {
		this.generateGame();
	}

	Restart() {
		this._game = [];
		this.Start();
	}

	Test() {
		this._users = [
			{
				user_name: "First",
				user_id: 1
			},
			{
				user_name: "Second",
				user_id: 2
			},
			{
				user_name: "Third",
				user_id: 3
			}
		];
		return this.Start();
	}
}

module.exports = WhoAmIGame;

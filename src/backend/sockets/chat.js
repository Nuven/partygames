let chatSocket = function(io, socket) {
	socket.on("user send a message", data => {
		io.emit("add message", data);
	});
};

module.exports = chatSocket;

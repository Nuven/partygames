let spyfallGame = require("../../services/games/spyfall");
// let spyfallSocket = function(io, socket) {
// 	socket.on("start the game", data => {
// 		socket.game.logic = new spyfallGame(socket.game.users);
// 		socket.game.logic.Start();
// 		console.log(socket.game.logic.getGame());
// 		socket.game.started = true;
// 		console.log("GAGAGAG", socket.game.logic.getGame());
// 		io.to("game_" + data.game_id).emit("start game", socket.game);
// 	});
// };

class spyfallSocket {
	constructor(io, socket, game) {
		this._io = io;
		this._socket = socket;
		this._game = game;
	}

	Start() {
		this._game.logic = new spyfallGame(this._game.users);
		this._game.logic.Start();
	}

	getLocations() {
		return this._game.logic.getLocations();
	}

	Finish() {
		this._game.logic.Finish();
	}
}

module.exports = spyfallSocket;

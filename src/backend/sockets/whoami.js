let whoAmIGame = require("../../services/games/whoami");

class whoAmILogic {
	constructor(io, socket, game) {
		this._io = io;
		this._socket = socket;
		this._game = game;
	}

	init(io, socket) {
		io.to("game_" + socket.game_id).on("add badge to user", data => {
			console.log("TEETET");
			this._game.logic.setBadgeToUser(data.target_id, data.badge_text);
			io.to("game_" + socket.game_id).emit("update game", this._game);
		});
	}

	Start() {
		this._game.logic = new whoAmIGame(this._game.users);
		this._game.logic.Start();
	}

	Finish() {
		this._game.logic.Finish();
	}
}

function initwhoAmISocket(io, socket) {
	socket.on("add badge to user", data => {
		console.log("TEETET");
		socket.game.logic.setBadgeToUser(data.target_id, data.badge_text);
		io.to("game_" + socket.game_id).emit("update game", socket.game);
	});
}

module.exports = { whoAmILogic, initwhoAmISocket };

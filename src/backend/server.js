let app = require("express")();
let http = require("http").Server(app);
let io = require("socket.io")(http);

let chatSocket = require("./sockets/chat");
let { whoAmILogic, initwhoAmISocket } = require("./sockets/whoami");
let spyfallGameSocket = require("./sockets/spyfall");

let usersOnline = [];

let gamesAvailable = [];
let gamesEnabled = [];



io.on("connection", function(socket) {
	console.log("a user connected");
	console.log("USERS: ", usersOnline);

	socket.on("already logged", user => {
		socket.user = user;

		if (!containsObject(socket.user, usersOnline)) {
			usersOnline.push(socket.user);
			io.emit("user logged", user);
		}
	});

	socket.on("login", user => {
		socket.user = user;
		console.log("User logged with name: ", user.user_name);
		usersOnline.push(socket.user);
		io.emit("user logged", user);
		console.log(socket.user);
	});

	socket.emit("user connected", usersOnline, gamesAvailable);

	chatSocket(io, socket);

	socket.on("game was created", game => {
		io.emit("add game to list", game);
		gamesAvailable.push(game);
	});

	socket.on("user enter the game", data => {
		socket.join("game_" + data.game_id);
		socket.game_id = data.game_id;
		socket.game = gamesAvailable.find(el => el.game_id === data.game_id);
		socket.game.users.push(data.user);
		console.log(socket.game);
		io.to("game_" + data.game_id).emit(
			"add user to game",
			socket.game,
			gamesAvailable
		);
	});

	socket.on("user leave the game", data => {
		let game = getGameById(data.game_id, gamesAvailable);

		if (game) removeUserFromArray(socket.user, game.users);

		io.to("game_" + data.game_id).emit("update game", game);
		socket.leave("game_" + data.game_id);
	});

	socket.on("start the game", data => {
		socket.game.start = true;
		socket.game.started = true;
		gamesEnabled.push(socket.game);

		let game = [];
		switch(socket.game.game_name) {
		case  "whoami":
			game = new whoAmILogic(io, socket, socket.game);
			game.Start();
			break;
		case "spyfall":
			game = new spyfallGameSocket(io, socket, socket.game);
			game.Start();
			socket.game.locations = game.getLocations();
			break;
		default:
			break;
		}

		console.log("ENABLED: ", gamesEnabled);
		io.emit("update games", gamesAvailable);
		io.to("game_" + data.game_id).emit("start game", socket.game);
	});

	initwhoAmISocket(io, socket);

	socket.on("delete the game", ({ game_id }) => {
		let game = getGameById(game_id, gamesAvailable);
		removeGameFromArray(game, gamesAvailable);
		io.emit("update games", gamesAvailable);
		io.emit("remove game");
	});

	socket.on("logout", user => {
		usersOnline = usersOnline.filter(
			item => JSON.stringify(item) !== JSON.stringify(user)
		);
		console.log("User logout", usersOnline);
		io.emit("user logged out", user);
	});

	socket.on("disconnect", () => {
		console.log("a user disconnected", usersOnline);
		//io.emit("user logged out", socket.user);
	});
});

http.listen(3000, function() {
	console.log("listening on *:3000");
});

function removeGameFromArray(game, gameArray) {
	console.log(game.game_id);
	for (let i = 0; i < gameArray.length; i++) {
		if (gameArray[i].game_id === game.game_id) gameArray.splice(i, 1);
	}
}

function removeUserFromArray(user, array) {
	for (let i = 0; i < array.length; i++) {
		if (array[i].user_id === user.user_id) array.splice(i, 1);
	}
}

function getGameById(game_id, gameArray) {
	for (let i = 0; i < gameArray.length; i++) {
		if (gameArray[i].game_id === game_id) return gameArray[i];
	}

	return false;
}

function containsObject(obj, list) {
	for (let item of list) {
		if (JSON.stringify(item) === JSON.stringify(obj)) return true;
	}
	return false;
}
